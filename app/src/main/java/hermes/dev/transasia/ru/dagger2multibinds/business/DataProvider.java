package hermes.dev.transasia.ru.dagger2multibinds.business;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class DataProvider {

    @Inject
    DataProvider(){}

    public List<String> getTaxerZone() {
        List<String> list = new ArrayList<>();

        list.add("east");
        list.add("center");
        list.add("west");

        return list;
    }

    public List<String> getPaymentMethod() {
        List<String> list = new ArrayList<>();

        list.add("credit");
        list.add("debit");
        list.add("paypal");

        return list;
    }

    public String getOrderId() {
        return "23747324";
    }


    public double getPrice() {
        return 234.0;
    }


}
