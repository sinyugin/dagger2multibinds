package hermes.dev.transasia.ru.dagger2multibinds.business;

import javax.inject.Inject;

public class CenterTaxer implements Taxer {

    @Inject
    CenterTaxer(){}

    @Override
    public double getPriceWithTax(double price) {
        return price * 0.4;
    }
}
