package hermes.dev.transasia.ru.dagger2multibinds;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import javax.inject.Inject;

import hermes.dev.transasia.ru.dagger2multibinds.business.DataProvider;
import hermes.dev.transasia.ru.dagger2multibinds.business.ViewModel;
import hermes.dev.transasia.ru.dagger2multibinds.di.DaggerAppComponent;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "dagger2+Me";

    @Inject
    ViewModel viewModel;

    @Inject
    DataProvider dataProvider;

    private Spinner taxerSpinner;
    private Spinner paymentSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DaggerAppComponent.create().inject(this);

        Log.d(TAG, "onCreate: " + viewModel.getMsg());

        initView();

        setupPaymentAdapter();

        setupTaxerApinner();
    }

    private void setupPaymentAdapter() {
        ArrayAdapter<String> paymentAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataProvider.getPaymentMethod());
        paymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        paymentSpinner.setAdapter(paymentAdapter);

        paymentSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                int chosetItemPosition = adapterView.getSelectedItemPosition();
                String paymentInfo = viewModel.providePaymentInfo(chosetItemPosition, dataProvider.getOrderId());

                Log.d(TAG, "onItemSelected: " + adapterView.getSelectedItem().toString() + " " + adapterView.getSelectedItemPosition() + " " + paymentInfo);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setupTaxerApinner() {
        ArrayAdapter<String> taxerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, dataProvider.getTaxerZone());
        taxerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        taxerSpinner.setAdapter(taxerAdapter);

        taxerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String chosenItem = adapterView.getSelectedItem().toString();
                double price = viewModel.calculateTax(chosenItem, dataProvider.getPrice());
                Log.d(TAG, "onItemSelected: " + chosenItem + " " + price);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void initView() {
        taxerSpinner = (Spinner) findViewById(R.id.taxerSpinner);
        paymentSpinner = (Spinner) findViewById(R.id.paymentSpinner);
    }
}
