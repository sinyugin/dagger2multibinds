package hermes.dev.transasia.ru.dagger2multibinds.business;

import javax.inject.Inject;

public class CreditPayment implements Payment {

    @Inject
    CreditPayment(){}

    @Override
    public String getPaymentInfo(String orderId) {
        return "Кредитная карта";
    }
}
