package hermes.dev.transasia.ru.dagger2multibinds.business;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

public class ViewModel {

    @Inject
    public ViewModel() {
        msg = "Try like that";
    }

    @Inject
    Map<String, Taxer> taxerMap;

    @Inject
    Set<Payment> paymentSet;

    private String msg;

    public String getMsg() {
        return msg;
    }

    public double calculateTax(String zone, double price) {
        return taxerMap.get(zone).getPriceWithTax(price);
    }


    public String providePaymentInfo(int chosetItemPosition, String orderId) {
        return (new ArrayList<Payment>(paymentSet)).get(chosetItemPosition).getPaymentInfo(orderId);
    }
}
