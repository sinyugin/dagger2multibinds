package hermes.dev.transasia.ru.dagger2multibinds.business;

import javax.inject.Inject;

public class EastTaxer implements Taxer {

    @Inject
    EastTaxer(){}

    @Override
    public double getPriceWithTax(double price) {
        return price * 0.2;
    }
}
