package hermes.dev.transasia.ru.dagger2multibinds.business;

import javax.inject.Inject;

public class WestTaxer implements Taxer {

    @Inject
    WestTaxer(){}

    @Override
    public double getPriceWithTax(double price) {
        return price * 0.6;
    }
}
