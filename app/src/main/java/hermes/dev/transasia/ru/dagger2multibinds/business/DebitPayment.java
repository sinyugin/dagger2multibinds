package hermes.dev.transasia.ru.dagger2multibinds.business;

import javax.inject.Inject;

public class DebitPayment implements Payment {

    @Inject
    DebitPayment(){}


    @Override
    public String getPaymentInfo(String orderId) {
        return "Дебетовая карта";
    }
}
