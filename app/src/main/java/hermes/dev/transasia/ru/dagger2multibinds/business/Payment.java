package hermes.dev.transasia.ru.dagger2multibinds.business;

public interface Payment {

    String getPaymentInfo(String orderId);
}
