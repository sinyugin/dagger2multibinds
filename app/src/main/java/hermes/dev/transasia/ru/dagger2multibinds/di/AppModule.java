package hermes.dev.transasia.ru.dagger2multibinds.di;


import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;
import dagger.multibindings.IntoSet;
import dagger.multibindings.StringKey;
import hermes.dev.transasia.ru.dagger2multibinds.business.CenterTaxer;
import hermes.dev.transasia.ru.dagger2multibinds.business.CreditPayment;
import hermes.dev.transasia.ru.dagger2multibinds.business.DebitPayment;
import hermes.dev.transasia.ru.dagger2multibinds.business.EastTaxer;
import hermes.dev.transasia.ru.dagger2multibinds.business.Payment;
import hermes.dev.transasia.ru.dagger2multibinds.business.PaypalPayment;
import hermes.dev.transasia.ru.dagger2multibinds.business.Taxer;
import hermes.dev.transasia.ru.dagger2multibinds.business.WestTaxer;

@Module
public abstract class AppModule {

    @Binds
    @IntoSet
    public abstract Payment getDebit(DebitPayment debitPayment);

    @Binds
    @IntoSet
    public abstract Payment getCredit(CreditPayment creditPayment);

    @IntoSet
    @Binds
    public abstract  Payment getPaypal(PaypalPayment paypalPayment);

    @Binds
    @IntoMap
    @StringKey("east")
    public abstract Taxer getEastZone(EastTaxer eastTaxer);

    @Binds
    @IntoMap
    @StringKey("center")
    public abstract Taxer getCenterZone(CenterTaxer centerTaxer);

    @Binds
    @IntoMap
    @StringKey("west")
    public abstract Taxer getWestZone(WestTaxer westTaxer);

}
