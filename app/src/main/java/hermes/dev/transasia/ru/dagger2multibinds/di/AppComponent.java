package hermes.dev.transasia.ru.dagger2multibinds.di;


import javax.inject.Singleton;

import dagger.Component;
import hermes.dev.transasia.ru.dagger2multibinds.MainActivity;

@Component (modules = {AppModule.class, SecondModule.class})
@Singleton
public interface AppComponent {

    void inject(MainActivity activity);
}
