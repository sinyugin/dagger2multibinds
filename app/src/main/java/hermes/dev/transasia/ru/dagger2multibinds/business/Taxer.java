package hermes.dev.transasia.ru.dagger2multibinds.business;

public interface Taxer {

    double getPriceWithTax(double price);
}
